resource "aws_lb" "lb-router" {
  name               = "lb-router-${var.student_id}"
  security_groups    = ["${aws_security_group.sg-fe.id}"]
  internal           = false
  load_balancer_type = "application"

  subnets = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  tags {
    Name = "lb-router-${var.student_id}"
  }
}

  resource "aws_lb_target_group" "tg-router" {
  name        = "tg-router-${var.student_id}"
  port        = "1972"
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.vpc.id}"
  target_type = "instance"
}

resource "aws_lb_listener" "listen-router" {
  load_balancer_arn = "${aws_lb.lb-router.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.tg-router.arn}"
    type             = "forward"
  }
}




data "template_file" "router-cloud-init" {
  template = "${file("${path.module}/templates/router-cloud-init.tpl")}"

  vars {
    router_service_bucket      = "${var.s3_bucket}"
    router_tarball             = "m2m.router.tgz"
    aws_access_key_id          = "${var.aws_access_key_id}"
    aws_secret_access_key      = "${var.aws_secret_access_key}"
  }
}

resource "aws_launch_configuration" "lc-router" {
  name_prefix                 = "lc-router-${var.student_id}"
  image_id                    = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.keypair.key_name}"
  security_groups             = ["${aws_security_group.sg-apps.id}"]
  associate_public_ip_address = true

  user_data = "${data.template_file.router-cloud-init.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg-router" {
  name                 = "asg-router-${var.student_id}"
  launch_configuration = "${aws_launch_configuration.lc-router.id}"

  vpc_zone_identifier = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  min_size     = 2
  max_size     = 2
  force_delete = true

  target_group_arns = ["${aws_lb_target_group.tg-router.arn}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Owner"
    value               = "${var.student_id}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "asg-router-${var.student_id}"
    propagate_at_launch = false
  }
}

output "router-dns" {
  value = "${aws_lb.lb-router.dns_name}"
}
