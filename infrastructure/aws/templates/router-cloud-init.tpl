#cloud-config
package_upgrade: true

packages:
  - awscli
  - nodejs
  - npm

runcmd:
  - echo "Hello from script"
  - mkdir -p /home/ubuntu/app
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_tarball}" /home/ubuntu/app/${router_tarball}
  - cd /home/ubuntu/app
  - tar xzf ${router_tarball}
  - cd /home/ubuntu/app/m2m.router
  - node index.js