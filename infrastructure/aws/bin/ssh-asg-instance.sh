#! /bin/bash
REGION=$1
ASG_NAME=$2

if [ -z "$REGION" ] 
then
    echo "You must provide a region as the first argument"
    exit 1
fi

if [ -z "$ASG_NAME" ]
then
    echo "You must provide an ASG name as the second argument"
    exit 1
fi

INSTANCE_IDS=`aws autoscaling describe-auto-scaling-groups --region ${REGION} --auto-scaling-group-names ${ASG_NAME} --query 'AutoScalingGroups[*].Instances[*].{instance:InstanceId}' --output text`

idarray=($INSTANCE_IDS)

PUBLIC_IP=`aws ec2 describe-instances --region ${REGION} --instance-id ${idarray[0]} --query "Reservations[*].Instances[*].PublicIpAddress" --output=text`

ssh ubuntu@${PUBLIC_IP}
