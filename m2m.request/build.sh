#! /bin/bash
docker run -it --rm -v maven-repo:/root/.m2 -v `pwd`:/usr/src/mymaven -w /usr/src/mymaven maven:3.5.3-jdk-8-alpine mvn clean package
docker build .